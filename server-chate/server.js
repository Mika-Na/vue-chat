const express = require('express');
const http = require('http');
const { Server } = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "*",
        methods: ['GET', "POST"]
    }
});


io.on('connection', (socket) => {

    socket.on('message', data => {
        // console.log({data});
        socket.broadcast.emit('message:received', data);
    })
    socket.on('chat', data =>{
        socket.broadcast.emit('chat:user', data);
        // console.log({data});
    })
})

server.listen(3000, () => {
    console.log('Chat est en marche sur le port 3000')
})
