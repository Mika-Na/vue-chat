import { createStore } from 'vuex'

export default createStore({
  state: {
    curentUser: "",
    allUser: [
      { userName : '' , id : ''}
    ],
    messages: [{
      msgCurentUser: [],
      msgOtherUser: [],
    }]
  },
  getters: {
    getUser(state) {
      return state.allUser;
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
