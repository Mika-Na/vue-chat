import { createRouter, createWebHistory } from 'vue-router'
import chat from '../components/chat'
import home from '../components'

const routes = [
  {
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/chat',
    name: 'chat',
    component: chat
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
